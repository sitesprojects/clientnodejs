var request = require('request');
var apiHost = process.env.APIURL || 'http://localhost:5000'

var appRouter = function(app) {

    app.get("/", function(req, res) {

        /*Captura de Temp*/
        temp = -1;

        request(apiHost+'/', { json: true }, (err, apiRes, body) => {
            if (err) { return console.log(err); }
            var response = {
                "success": "Api says : "+body.success ,
                "error": "",
                "data": [{ "temp": temp}]
            }

            res.send(response);
        })

    });

}

module.exports = appRouter;
